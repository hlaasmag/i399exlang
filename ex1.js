'use strict';

var array = [1, 2, 3, 4, 5, 6];

var numbersDivideableBy3 = array.filter(function(i) {
    return i %3 === 0;
});



console.log('Divideable by 3: ' + numbersDivideableBy3);
