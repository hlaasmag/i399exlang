'use strict';

/*var person = {
    name: 'Hando',
    getName : function () {
        console.log('Name is: ' + this.name);
    }
};*/

function Person(name) {

    this.name = name;


    this.getName = function () {
        console.log('Name is: ' + this.name);
    }
}

var hando =  new Person("Hando");
hando.getName();


function Truck(number) {

    this.number = number;
}

Truck.prototype.getNumber = function () {
    console.log('Truck number is: ' + this.number);
}


var volvo =  new Truck("RN3");
volvo.getNumber();

console.log(Object.keys(hando));
console.log(Object.keys(volvo.__proto__));


